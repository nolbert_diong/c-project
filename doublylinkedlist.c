#include <stdio.h>
#include <stdlib.h>

struct Node{
    int val;
    struct Node* prev;
    struct Node* next;
};

struct Node* head;

struct Node* GetNode(int x){
    struct Node* newData = (struct Node*)malloc(sizeof(struct Node));
    newData->val = x;
    newData->prev = NULL;
    newData->next = NULL;

    return newData;
}


void AddHead(int x){
    struct Node* newData = GetNode(x);
    printf("Add in Head-> ");
    if(head == NULL){
        head = newData;
        return;
    }
    head->prev = newData;
    newData->next = head;
    head = newData;
}


void AddTail(int x){
    struct Node* temp = head;
    struct Node* newData = GetNode(x);
    printf("Add in Tail-> ");
    if(head == NULL){
        head = newData;
        return;
    }
    while(temp->next != NULL){
        temp = temp->next;
    }
        temp->next = newData;
        newData->prev = temp;
        //temp->prev = temp;
}

void RemoveHead(){
    struct Node* temp = head;
    printf("Remove Head-> ");
    if(head == NULL){
        printf("Nothing to Remove!");
    }else{
        head = head->next;
    }
    free(temp);
}

void RemoveTail(){
    struct Node* temp = head;
    struct Node *del;
    printf("Remove Tail-> ");
    if(head == NULL){
        printf("Nothing to Remove!");
    }else{
        while(temp->next != NULL){
            del = temp;
            temp = temp->next;
        }
        free(del->next);
        del->next = NULL;
    }
}

void Print(){
    struct Node* temp = head;
    while(temp != NULL){
        printf("%d ", temp->val);
        temp = temp->next;
    }
    printf("\n");
}

int main(){
    head = NULL;
    AddHead(23);Print();
    AddHead(5);Print();
    AddTail(9);Print();
    AddTail(10);Print();
    AddHead(1);Print();
    AddHead(6);Print();
    AddHead(11);Print();
    AddTail(19);Print();
    RemoveHead();Print();
    AddHead(20);Print();
    RemoveTail();Print();
}
