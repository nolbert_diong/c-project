#include <stdio.h>
#include <stdlib.h>

struct Node{
    int val;
    struct Node* next;
};

struct Node* head = NULL;
struct Node* tail = NULL;


void Enqueue(int x){
    struct Node* temp = (struct Node*)malloc(sizeof(struct Node));
    temp->val = x;
    temp->next = NULL;
    printf("Enqueue->");
    if(head == NULL && tail == NULL){
        head = tail = temp;
        return;
    }
    tail->next = temp;
    tail = temp;
}

void Dequeue(){
    struct Node* temp = head;
    printf("Dequeue->");
    if(head == NULL){
        printf("Queue is Empty!");
    }else{
        head = head->next;
    }
    free(temp);
}

void Print(){
    struct Node* temp = head;
    while(temp != NULL){
        printf("%d ",temp->val);
        temp = temp->next;
    }
    printf("\n");
}

int main(){
    Enqueue(10);Print();
    Enqueue(20);Print();
    Enqueue(15);Print();
    Dequeue();Print();
    Enqueue(30);Print();
}
