#include <stdio.h>
#include <stdlib.h>

int Partition (int array[], int left, int right){
    int Pivot = array[right];
    int i = (left - 1);

    for (int j = left; j <= right- 1; j++){
        if (array[j] < Pivot){
            i++;
            Swap(&array[i], &array[j]);
        }
    }
    Swap(&array[i + 1], &array[right]);

    return (i + 1);
}

void QuickSort(int array[], int left, int right){
    if (left < right){

        int p = Partition(array, left, right);
        QuickSort(array, left, p - 1);
        QuickSort(array, p + 1, right);
    }
}


void Swap(int *a, int *b){
    int t = *a;
    *a = *b;
    *b = t;
}

void Print(int array[], int size){
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", array[i]);
    printf("\n");
}

int main(){
    int array[] = {50, 12, 1, 63, 52, 23};
    int n = sizeof(array)/sizeof(array[0]);

    printf("Given array: \n");
    Print(array, n);

    QuickSort(array, 0, n-1);
    printf("\nSorted array: \n");
    Print(array, n);

    return 0;
}
