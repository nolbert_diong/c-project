#include <stdio.h>
#include <stdlib.h>

void SelectionSort(int array[], int n){
    int i;
    int j;
    int min;

    for (i = 0; i < n - 1; i++){
        min = i;
        for (j = i+1; j < n; j++){
          if (array[j] < array[min]){
            min = j;
          }
        }
        swap(&array[min], &array[i]);
    }
}

void swap(int *a, int *b){
    int t = *a;
    *a = *b;
    *b = t;
}

void Print(int array[], int size){
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", array[i]);
        printf("\n");
}

int main(){
    int array[] = {1, 10, 50, 20, 11, 25};
    int n = sizeof(array)/sizeof(array[0]);
    printf("Given array: \n");
    Print(array, n);

    SelectionSort(array, n);
    printf("\nSorted array: \n");
    Print(array, n);

    return 0;
}
