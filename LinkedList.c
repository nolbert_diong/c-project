#include <stdio.h>
#include <stdlib.h>


struct Node{
    int val;
    struct Node* next;
};

struct Node* head;

void AddHead(int x){
    struct Node* newData = (struct Node*)malloc(sizeof(struct Node));
    printf("Add in Head-> ");
    newData->val = x;
    newData->next = head;
    head = newData;
}

void AddTail(int x){
    struct Node* temp = head;
    struct Node* newData = (struct Node*)malloc(sizeof(struct Node));
    newData->val = x;
    printf("Add in Tail-> ");
    if(head == NULL){
        head = newData;
        return;
    }
    while(temp->next != NULL){
        temp = temp->next;
    }
    temp->next = newData;
    newData->next = NULL;
}

void RemoveHead(){
    struct Node* temp = head;
    printf("Remove in Head-> ");
    if(head == NULL){
        printf("Nothing to Remove!");
    }else{
        head = head->next;
    }
    free(temp);
}

void RemoveTail(){
    struct Node *temp =head;
    struct Node *del;
    printf("Remove in Tail-> ");
    while(temp->next != NULL){
        del = temp;
        temp = temp->next;
    }
    free(del->next);
    del->next=NULL;
}

void Print(){
    struct Node* temp = head;
    while(temp != NULL){
        printf("%d ",temp->val);
        temp = temp->next;
    }
    printf("\n");
}

int main(){
    head = NULL;
    AddHead(50),Print();
    AddHead(10),Print();
    AddTail(30),Print();
    AddTail(20),Print();
    RemoveHead(),Print();
    RemoveTail(),Print();
    AddHead(12),Print();
    AddTail(13),Print();

    return 0;
}
