#include <stdio.h>
#include <stdlib.h>

void Merge(int array[], int left, int mid, int right){
    int i;
    int j;
    int k;
    int n1 = mid - left + 1;
    int n2 =  right - mid;

    int Left[n1], Right[n2];

    for (i = 0; i < n1; i++){
        Left[i] = array[left + i];
    for (j = 0; j < n2; j++){
        Right[j] = array[mid + 1+ j];
    }
    }
    i = 0;
    j = 0;
    k = left;
    while (i < n1 && j < n2){
        if (Left[i] <= Right[j]){
            array[k] = Left[i];
            i++;
        }else{
            array[k] = Right[j];
            j++;
        }
        k++;
    }

    while (i < n1){
        array[k] = Left[i];
        i++;
        k++;
    }

    while (j < n2){
        array[k] = Right[j];
        j++;
        k++;
    }
}

void MergeSort(int array[], int left, int right){
    if (left < right){

        int m = left + (right - left)/2;

        MergeSort(array, left, m);
        MergeSort(array, m + 1, right);
        Merge(array, left, m, right);
    }
}

void Print(int array[], int size){
    int i;
    for (i = 0; i < size; i++)
        printf("%d ", array[i]);
    printf("\n");
}

int main(){
    int array[] = {23, 10, 5, 2, 1, 30};
    int arraysize = sizeof(array)/sizeof(array[0]);

    printf("Given array is: \n");
    Print(array, arraysize);

    MergeSort(array, 0, arraysize - 1);

    printf("\nSorted array is: \n");
    Print(array, arraysize);
    return 0;
}
