#include <stdio.h>
#include <stdlib.h>

void BubbleSort(int array[], int n){
   int i;
   int j;
   for (i = 0; i < n-1; i++){

       for (j = 0; j < n-i-1; j++){
           if (array[j] > array[j+1]){
              swap(&array[j], &array[j+1]);
           }
       }
   }
}

void swap(int *a, int *b){
    int t = *a;
    *a = *b;
    *b = t;
}

void Print(int array[], int size){
    int i;
    for (i = 0; i < size; i++){
        printf("%d ", array[i]);
    }
    printf("\n");
}

int main(){
    int array[] = {64, 34, 25, 12, 22, 11, 90};
    int n = sizeof(array)/sizeof(array[0]);
    printf("Given array: \n");
    Print(array, n);

    BubbleSort(array, n);
    printf("\nSorted array: \n");
    Print(array, n);

    return 0;
}
